package com.example.class_activity_7;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
DatePicker datePicker;
ImageButton pinlocation;
    private GpsTracker gpsTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        datePicker=findViewById(R.id.simpleDatePicker);
        pinlocation=findViewById(R.id.pin_location);
        pinlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(MainActivity.this, UserDetails.class);
                gpsTracker=new GpsTracker(MainActivity.this);

if(gpsTracker.canGetLocation())
{
    SharedPreferences prefs=getSharedPreferences("MySharedPref",MODE_PRIVATE);
    SharedPreferences.Editor myEdit =prefs.edit();
    String lon=String.valueOf(gpsTracker.getLongitude());
    String lat=String.valueOf(gpsTracker.getLatitude());
    myEdit.putString("latitude",lat);
    myEdit.putString("longitude",lon);
    myEdit.putString("day",String.valueOf(datePicker.getDayOfMonth()));
    myEdit.putString("month",String.valueOf(datePicker.getMonth()+1));
    myEdit.putString("year",String.valueOf(datePicker.getYear()));
    myEdit.commit();

    startActivity(intent);




}
else
    gpsTracker.showSettingsAlert();




            }
        });
    }
}